import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Empleado } from 'src/app/models/empleado';
import { Departamento } from 'src/app/models/departamento.enum';

@Component({
  selector: 'app-lista-empleados',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './lista-empleados.component.html',
  styleUrls: ['./lista-empleados.component.sass']
})
export class ListaEmpleadosComponent implements OnInit {
  objectKeys = Object.keys;
  empleados: Empleado[] = [];
  mostrar: boolean;

  nombre: string;
  apellidos: string;
  departamento: Departamento;
  departamentos: any = {}; //= Object.keys(Departamento);

  constructor() { 
    this.mockData();

  }

  ngOnInit(): void {
    const keys  = Object.keys(Departamento);
    const vals = Object.values(Departamento);

    for (let i = 0; i < keys.length ; i++ ) {
      this.departamentos[keys[i]] = vals[i];
    }
  }

  // Move this to a singleton class/Service where a 1! copy of the data is stored?
  mockData() {
    this.empleados.push(new Empleado('Pepe', 'García García', Departamento.FINANZAS));
    this.empleados.push(new Empleado('Andrés', 'García García', Departamento.IT));
    this.empleados.push(new Empleado('Pepe', 'García Suarez', Departamento.RECURSOS_HUMANOS));
    this.empleados.push(new Empleado('Paco', 'Alonso García', Departamento.FINANZAS));
  }

  toggleFormulario() { 
    this.mostrar = !this.mostrar;
  }

  crearEmpleado() {
    const empleado = new Empleado(this.nombre, this.apellidos, this.departamento as Departamento);
    this.empleados.push(empleado);
  }

  disabled() { 
    return this.nombre == null || this.apellidos == null || this.departamento == null;
  }

  borrarEmpleado(event:any, empleado: Empleado) {
    console.log(event);
    console.log(empleado);
    const indiceABorrar = this.empleados.indexOf(empleado);
    if (indiceABorrar >= 0) {
      this.empleados.splice(indiceABorrar, 1);
    }
  }

}

