import { Departamento } from './departamento.enum';

export class Empleado {
  nombre: string;
  apellidos: string;
  departamento: Departamento;

  constructor(nombre: string, apellidos: string, departamento: Departamento) { 
    this.nombre = nombre;
    this.apellidos = apellidos;
    this.departamento = departamento;

  }
}
